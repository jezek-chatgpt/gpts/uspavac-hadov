import random
import string
choices = (string.ascii_lowercase + 30*' ' + ',-')
fields = [
  ("Postavy", 5, 20),
  ("Dej", 4, 10),
  ("Miesto diania", 3, 9),
  ("Čas diania", 2, 6),
  ("Tématika", 3, 15),
  ("Konflikt", 5, 15),
  ("Perspektíva rozprávania", 3, 9),
  ("Žáner", 3, 10),
  ("Motívy", 3, 7)
]
form = {field: ''.join(random.choices(choices, k=random.randint(min_len, max_len))) for field, min_len, max_len in fields}
print(form)
